#!/bin/bash
kubectl create -f deploy/crds/lab_v1_nfsclient_crd.yaml
operator-sdk --image-builder=buildah build registry.gitlab.com/clintea/nfs-client-operator:v1
podman login -uclintea -p5yjqJRxBdZFkga1OT8at registry.gitlab.com
podman push registry.gitlab.com/clintea/nfs-client-operator:v1
kubectl create -f deploy/service_account.yaml
kubectl create -f deploy/role.yaml
kubectl create -f deploy/role_binding.yaml
kubectl create -f deploy/operator.yaml
kubectl apply -f deploy/crds/lab_v1_nfsclient_cr.yaml
oc adm policy add-scc-to-user hostmount-anyuid system:serviceaccount:lab:nfs-client-provisioner
