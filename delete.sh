#!/bin/bash
kubectl delete -f deploy/crds/lab_v1_nfsclient_cr.yaml
kubectl delete -f deploy/operator.yaml
kubectl delete -f deploy/role_binding.yaml
kubectl delete -f deploy/role.yaml
kubectl delete -f deploy/service_account.yaml
kubectl delete -f deploy/crds/lab_v1_nfsclient_crd.yaml
